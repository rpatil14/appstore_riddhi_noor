import random
import sys
import base64
import re
import os
from os.path import basename
from urllib.request import urlopen
from zipfile import ZipFile

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.urls import reverse

from apps.models import Release, App, Author, OrderedAuthor
from util.id_util import fullname_to_name
from util.view_util import html_response, json_response, get_object_or_none
from .models import AppPending
from .processjar import process_jar


# Presents an app submission form and accepts app submissions.
@login_required
def submit_app(request):
    context = dict()
    if request.method == 'POST':
        expect_app_name = request.POST.get('expect_app_name')
        f = request.FILES.get('file')
        f = request.POST.get('url_val', None) if f is None else f
        if f:
            try:
                jar_details = process_jar(f, expect_app_name)
                pending = _create_pending(request.user, jar_details, f)
                version_pattern ="^[0-9].[0-9].[0-9]+"
                version_pattern = re.compile(version_pattern)
                if not bool(version_pattern.match(jar_details['version'])):
                    raise ValueError("The version is not in proper pattern. It should have 3 order version numbering "
                                     "(e.g: x.y.z)")
                if jar_details['has_export_pkg']:
                    return HttpResponseRedirect(reverse('submit-api', args=[pending.id]))
                else:
                    return HttpResponseRedirect(reverse('confirm-submission', args=[pending.id]))
            except ValueError as e:
                context['error_msg'] = str(e)
    else:
        expect_app_name = request.GET.get('expect_app_name')
        if expect_app_name:
            context['expect_app_name'] = expect_app_name
    return html_response('upload_form.html', context, request)


def _user_cancelled(request, pending):
    pending.delete_files()
    pending.delete()
    return HttpResponseRedirect(reverse('submit-app'))


def _user_accepted(request, pending):
    app = get_object_or_none(App, name = fullname_to_name(pending.fullname))
    if app:
        if not app.is_editor(request.user):
            return HttpResponseForbidden('You are not authorized to add releases, because you are not an editor')
        if not app.active:
            app.active = True
            app.save()
        pending.make_release(app)
        pending.delete_files()
        pending.delete()
        return HttpResponseRedirect(reverse('app_page_edit', args=[app.name]) + '?upload_release=true')
    else:
        return html_response('submit_done.html', {'app_name': pending.fullname}, request)


def confirm_submission(request, id):
    pending = get_object_or_404(AppPending, id=int(id))
    if not pending.can_confirm(request.user):
        return HttpResponseRedirect('/')
    pending_obj = AppPending.objects.filter(symbolicname=pending.symbolicname, version=pending.version)
    is_pending_replace = True if pending_obj.count() > 1 else False
    action = request.POST.get('action')
    if action:
        latest_pending_obj_ = pending_obj[1] if is_pending_replace else pending_obj[0]
        if action == 'cancel':
            return _user_cancelled(request, latest_pending_obj_)
        elif action == 'accept':
            if pending_obj.count() > 1:
                _replace_jar_details(request, pending_obj)
            server_url = _get_server_url(request)
            _send_email_for_pending(server_url, latest_pending_obj_)
            _send_email_for_pending_user(latest_pending_obj_)
            return _user_accepted(request, latest_pending_obj_)
    return html_response('confirm.html',{'pending': pending,
                         'is_pending_replace': is_pending_replace}, request)

def _create_pending(submitter, jar_details, release_file):
    pending = AppPending.objects.create(submitter       = submitter,
                                        symbolicname    = jar_details['symbolicname'],
                                        details         = base64.b64decode(jar_details['details']).decode('utf-8'),
                                        fullname        = jar_details['fullname'],
                                        version         = jar_details['version'],
                                        repository      = jar_details['repository'])

    file, file_name = _get_jar_file(release_file)
    pending.release_file.save(basename(str(random.randrange(sys.maxsize)) + "_" + file_name), file)
    pending.release_file_name = file_name
    pending.save()
    if isinstance(release_file, str):
        os.remove(file_name)
    return pending


def _replace_jar_details(request, pending_obj):
    """
    The function replaces the existing pending app details with the latest jar details
    if the jar is not yet released else replaces the released app details with the latest
    app details.
    :param request:
    :param pending_obj:
    :return:
    """
    latest_pending_obj = pending_obj[pending_obj.count() - 1]
    existing_pending_obj = pending_obj[pending_obj.count() - 2]
    if latest_pending_obj and latest_pending_obj.submitter != request.user:
        raise ValueError('cannot be accepted because you are not an editor')
    name = fullname_to_name(latest_pending_obj.fullname)
    existing_pending_obj.details = latest_pending_obj.details
    existing_pending_obj.repository = latest_pending_obj.repository
    existing_pending_obj.fullname = latest_pending_obj.fullname
    existing_pending_obj.release_file = latest_pending_obj.release_file
    existing_pending_obj.save()
    latest_pending_obj.delete_files()
    latest_pending_obj.delete()


def _get_jar_file(release_file):
    """
    The function checks if the given file is a url. If yes, it reads the
    details into a temporary file and returns
    the file object and file name else returns the given file object.
    :param release_file:
    :return:
    """
    file_name = basename(release_file) if isinstance(release_file, str) else basename(release_file.name)
    if isinstance(release_file, str):
        url_data = urlopen(release_file).read()
        with open(file_name, 'wb') as file:
            file.write(url_data)
        file = open(file_name, 'rb')
    else:
        file = release_file
    return file, file_name


def _send_email_for_pending(server_url, pending):
    admin_url = reverse('admin:login', current_app=pending.fullname)
    msg = u"""
The following app has been submitted:
    ID: {id}
    Name: {fullname}
    Version: {version}
    Submitter: {submitter_name} {submitter_email}
    Server Url: {server_url}{admin_url}
""".format(id=pending.id, fullname=pending.fullname, version=pending.version, submitter_name=pending.submitter.username, submitter_email=pending.submitter.email, server_url=server_url, admin_url=admin_url)
    send_mail('{fullname} App - Successfully Submitted.'.format(fullname=pending.fullname), msg, settings.EMAIL_ADDR, settings.CONTACT_EMAILS, fail_silently=False)


def _send_email_for_pending_user(pending):
    msg = u"""
Thank you for submitting the app! {approve_text}
The following app has been submitted:
    Name: {fullname}
    Version: {version}
    Submitter: {submitter_name} {submitter_email}
""".format(approve_text="You'll be notified by email when your app has been approved." if pending.is_new_app else '',fullname = pending.fullname, version = pending.version, submitter_name = pending.submitter.username, submitter_email = pending.submitter.email)
    send_mail('{fullname} App - Successfully Submitted.'.format(fullname = pending.fullname), msg, settings.EMAIL_ADDR, [pending.submitter.email], fail_silently=False)


def _verify_javadocs_jar(file):
    error_msg = None
    file.open(mode = 'rb')
    try:
        zip = ZipFile(file, 'r')
        for name in zip.namelist():
            pathpieces = name.split('/')
            if name.startswith('/') or '..' in pathpieces:
                error_msg = 'The zip archive has a file path that is illegal: %s' % name
                break
        zip.close()
    except:
        error_msg = 'The Javadocs Jar file you submitted is not a valid jar/zip file'
    file.close()
    return error_msg

def _send_email_for_accepted_app(to_email, from_email, app_fullname, app_name, server_url):
    subject = u'IGB App Store - {app_fullname} Has Been Approved'.format(app_fullname = app_fullname)
    app_url = reverse('app_page', args=[app_name])
    msg = u"""Your app has been approved! Here is your app page:

  {server_url}{app_url}

To edit your app page:
 1. Go to {server_url}{app_url}
 2. Sign in as {author_email}
 3. Under the "Editor's Actions" on the top-right, choose "Edit this page".

Make sure to add some tags to your app and a short app description, which is located
right below the app name. You can also add screenshots, details about your app,
and an icon to make your app distinguishable.

If you would like other people to be able to edit the app page, have them sign in
to the App Store, then add their email addresses to the Editors box, located in
the top-right.

- IGB App Store Team
""".format(app_url = app_url, author_email = to_email, server_url = server_url)
    send_mail(subject, msg, from_email, (to_email,))


def _get_server_url(request):
    name = request.META['SERVER_NAME']
    port = request.META['SERVER_PORT']
    if port == '80':
        return 'http://%s' % name
    elif port == '443':
        return 'https://%s' % name
    else:
        return 'http://%s:%s' % (name, port)


def _pending_app_accept(pending, request):
    name = fullname_to_name(pending.fullname)
    # we always create a new app, because only new apps require accepting
    app = App.objects.create(fullname = pending.fullname, name = name)
    app.active = True
    app.symbolicname = pending.symbolicname
    app.details = pending.details
    app.version = pending.version
    app.editors.add(pending.submitter)
    app.repository = pending.repository
    app.save()

    pending.make_release(app)
    pending.delete_files()
    pending.delete()

    server_url = _get_server_url(request)
    _send_email_for_accepted_app(pending.submitter.email, settings.EMAIL_ADDR, app.fullname, app.name, server_url)


def _pending_app_decline(pending_app, request):
    pending_app.delete_files()
    pending_app.delete()


_PendingAppsActions = {
    'accept': _pending_app_accept,
    'decline': _pending_app_decline,
}

@login_required
def pending_apps(request):
    if not request.user.is_staff:
        return HttpResponseForbidden()
    if request.method == 'POST':
        action = request.POST.get('action')
        if not action:
            return HttpResponseBadRequest('action must be specified')
        if not action in _PendingAppsActions:
            return HttpResponseBadRequest('invalid action--must be: %s' % ', '.join(_PendingAppsActions.keys()))
        pending_id = request.POST.get('pending_id')
        if not pending_id:
            return HttpResponseBadRequest('pending_id must be specified')
        try:
            pending_app = AppPending.objects.get(id = int(pending_id))
        except AppPending.DoesNotExist as ValueError:
            return HttpResponseBadRequest('invalid pending_id')
        _PendingAppsActions[action](pending_app, request)
        if request.is_ajax():
            return json_response(True)

    pending_apps = AppPending.objects.all()
    return html_response('pending_apps.html', {'pending_apps': pending_apps}, request)


def _get_deploy_url(groupId, artifactId, version):
    return '/'.join((AppRepoUrl, groupId.replace('.', '/'), artifactId, version))


def _url_exists(url):
    try:
        reader = urlopen(url)
        if reader.getcode() == 200:
            return True
    except:
        pass
    return False


def artifact_exists(request):
    if request.method != 'POST':
        return HttpResponseBadRequest('no data')
    postLookup = request.POST.get
    groupId, artifactId, version = postLookup('groupId'), postLookup('artifactId'), postLookup('version')
    if not groupId or not artifactId or not version:
        return HttpResponseBadRequest('groupId, artifactId, or version not specified')
    deployUrl = _get_deploy_url(groupId, artifactId, version)
    return json_response(_url_exists(deployUrl))

#
# 2.x plugin management page
#


_PluginXmlUrl = 'http://chianti.ucsd.edu/cyto_web/plugins/plugins.xml'


def _forward_plugins_xml(request_post):
    try:
        reader = urlopen(_PluginXmlUrl)
        if reader.getcode() != 200:
            raise Error('retrieve failed')
        r = HttpResponse(content_type = 'application/xml')
        r.write(reader.read())
        return r
    except:
        return HttpResponse('Unable to retrieve: %s' % PluginXmlUrl, content_type='text/plain', status=503)


def _app_info(request_post):
    fullname = request_post.get('app_fullname')
    name = fullname_to_name(fullname)
    url = reverse('app_page', args=(name,))
    exists = App.objects.filter(name = name, active = True).count() > 0
    return json_response({'url': url, 'exists': exists})


def _update_app_page(request_post):
    fullname = request_post.get('fullname')
    if not fullname:
        return HttpResponseBadRequest('"fullname" not specified')
    name = fullname_to_name(fullname)
    app = get_object_or_none(App, name = name)
    if app:
        app.active = True
    else:
        app = App.objects.create(name = name, fullname = fullname)

    details = request_post.get('details')
    if details:
        app.details = details

    author_count = request_post.get('author_count')
    if author_count:
        author_count = int(author_count)
        for i in range(author_count):
            name = request_post.get('author_' + str(i))
            if not name:
                return HttpResponseBadRequest('no such author at index ' + str(i))
            institution = request_post.get('institution_' + str(i))
            author, _ = Author.objects.get_or_create(name = name, institution = institution)
            author_order = OrderedAuthor.objects.create(app = app, author = author, author_order = i)

    app.save()
    return json_response(True)
